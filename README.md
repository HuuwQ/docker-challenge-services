### Docker Challenge Services :

Créer un docker-compose.yml avec minimum 2 services (et maximum 5)

Créer parmi ces services une image "custom" via un Dockerfile
Tester la commande docker-compose up qui doit être capable de lancer tous les projets et ses différents services
Envoyer le projet (docker-compose.yml et dockerfile et code) sur un git (github/gitlab)
Pour demain rassembler les liens des git dans un fichier excel
Demain après midi démo commenté des projets avec suggestions d'améliorations/optimisations
